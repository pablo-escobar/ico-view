= ico-sxiv(1)

WARNING: This repository has been moved to https://git.escobar.life/ico-sxiv/log.html

== Synopsis

View the individual entries of an ICO file in `sxiv`.

== Requirements

* https://www.gnu.org/software/coreutils/[`mktemp`]
* https://github.com/muennich/sxiv[`sxiv`]
* https://imagemagick.org/index.php[ImageMagick]

== Usage

`ico-sxiv FILE`

== Installation

The `ico-sxiv(1)` command and it's man page can be installed in Unix systems 
by running:

----
$ curl -s https://gitlab.com/pablo-escobar/ico-view/-/raw/master/install.sh | sudo sh
----

The executable is installed in `$HOME/.local/bin/` and the man-page is 
installed in `$HOME/.local/share/man/man1/`.

== Authors

`ico-sxiv` was written by Pablo 
<pablo-escobar@riseup.net>.

== Copying

Copyright (C) 2020 Pablo.
Free use of this software is granted under the terms of the GPL-3.0 License.

