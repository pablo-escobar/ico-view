#!/bin/sh
#  _                          _       
# (_) ___ ___        _____  _(_)_   __
# | |/ __/ _ \ _____/ __\ \/ / \ \ / /
# | | (_| (_) |_____\__ \>  <| |\ V / 
# |_|\___\___/      |___/_/\_\_| \_/  
#                                     
# View the individual entries of an ICO file in `sxiv`.
# 
# Copyright (C) 2020 Pablo.
# Free use of this software is granted under the terms of the GPL-3.0 License.

error ()
{
    printf "\033[1m\033[38;5;9m[ERROR]\033[m %s\n\n" "$1"
    exit 1
}

dependencyError () 
{
    error "$(printf "The command '%s' could not be found.\nPlease make sure it is included in the \$PATH enviroment variable or follow the installation instructions on %s." "$1" "$2")"
}

if [ "$(id -u)" -ne "0" ]
then
    error "This script requires administrator-privileges"
fi

if ! [ -x "$(command -v mktemp)" ]
then
    dependencyError "mktemp" "https://www.gnu.org/software/coreutils/"
fi

if ! [ -x "$(command -v sxiv)" ]
then
    dependencyError "sxiv" "https://github.com/muennich/sxiv"
fi

if [ -f "$HOME/.local/bin/ico-sxiv" ]
then
    rm "$HOME/.local/bin/ico-sxiv"
fi

# Download the executable
wget https://gitlab.com/pablo-escobar/ico-view/-/raw/master/ico-sxiv -P "$HOME/.local/bin/"\
    && chmod +x "$HOME/.local/bin/ico-sxiv"\
    || exit 1

# Install the man page
tmp="$(mktemp -d)"
wget https://gitlab.com/pablo-escobar/ico-view/-/raw/master/ico-sxiv.1 -P "$tmp"\
    && install -g 0 -o 0 -m 0644 "$tmp/ico-sxiv.1" "$HOME/.local/share/man/man1/"\
    || exit 1

if [ -f "$HOME/.local/share/man/man1/ico-sxiv.1.gz" ]
then
    rm "$HOME/.local/share/man/man1/ico-sxiv.1.gz"
fi

gzip "$HOME/.local/share/man/man1/ico-sxiv.1"
rm "$tmp" -r

